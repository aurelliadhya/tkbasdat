from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from datetime import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

def index(request):
    response = {}
    return render(request, 'index.html', response)

def home(request):
    response = {}
    return render(request, 'index_petugas.html', response)

def homee(request):
    response = {}
    return render(request, 'index_admin.html', response)

def homeee(request):
    response = {}
    return render(request, 'index_anggota.html', response)

def penugasan_ptg(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT P.ktp, P.start_datetime, P.end_datetime, P.id_stasiun, S.nama AS nama_st, PE.nama FROM BIKE_SHARING.PENUGASAN AS P, BIKE_SHARING.STASIUN AS S, BIKE_SHARING.PERSON AS PE WHERE S.id_stasiun = P.id_stasiun AND P.ktp = PE.ktp;")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'penugasan petugas.html', response)

def acara_ptg(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.ACARA")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'acara petugas.html', response)

def acara_agt(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.ACARA")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'acara anggota.html', response)

def penugasan_adm(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT P.ktp, P.start_datetime, P.end_datetime, P.id_stasiun, S.nama AS nama_st, PE.nama FROM BIKE_SHARING.PENUGASAN AS P, BIKE_SHARING.STASIUN AS S, BIKE_SHARING.PERSON AS PE WHERE S.id_stasiun = P.id_stasiun AND P.ktp = PE.ktp;")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'penugasan admin.html', response)

def add_penugasan(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT nama, id_stasiun from BIKE_SHARING.STASIUN")
    daftarst = dictfetchall(cursor)
    response['daftarst'] = daftarst
    cursor.execute("SELECT P.ktp, PE.nama from BIKE_SHARING.PETUGAS AS P, BIKE_SHARING.PERSON AS PE WHERE P.ktp = PE.ktp;")
    daftarptg = dictfetchall(cursor)
    response['daftarptg'] = daftarptg
    return render(request, 'form penugasan admin.html', response)

@csrf_exempt
def add_png(request):
    if (request.method == 'POST'):
        cursor = connection.cursor()
        ktp = request.POST["petugas"]
        tgl_mulai = request.POST["tgl_mulai"]
        tgl_akhir = request.POST["tgl_akhir"]
        stasiun = request.POST["stasiun"]
        cursor.execute("INSERT INTO BIKE_SHARING.PENUGASAN (ktp,start_datetime,id_stasiun,end_datetime) VALUES('"+str(ktp)+"','"+tgl_mulai+"','"+str(stasiun)+"','"+tgl_akhir+"');")
    return redirect(reverse('penugasan_adm'))

def update_penugasan(request, ktp, tgl_mulai, stasiun):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM BIKE_SHARING.PENUGASAN WHERE ktp = '"+ktp+"' AND start_datetime = '"+tgl_mulai+"' AND id_stasiun = '"+stasiun+"';")
    update = dictfetchall(cursor)
    print(update)

    cursor.execute("SELECT nama, id_stasiun from BIKE_SHARING.STASIUN;")
    daftarst = dictfetchall(cursor)

    cursor.execute("SELECT id_stasiun FROM BIKE_SHARING.PENUGASAN where ktp = '"+ktp+"' AND start_datetime = '"+tgl_mulai+"' AND id_stasiun = '"+stasiun+"';")
    selected_stasiun = cursor.fetchone()

    cursor.execute("SELECT P.ktp, PE.nama from BIKE_SHARING.PETUGAS AS P, BIKE_SHARING.PERSON AS PE WHERE P.ktp = PE.ktp;")
    daftarptg = dictfetchall(cursor)

    cursor.execute("SELECT ktp FROM BIKE_SHARING.PENUGASAN where ktp = '"+ktp+"' AND start_datetime = '"+tgl_mulai+"' AND id_stasiun = '"+stasiun+"';")
    selected_ptg = cursor.fetchone()

    response['update'] = update[0]
    response['daftarst'] = daftarst
    response['selected_stasiun'] = selected_stasiun
    response['daftarptg'] = daftarptg
    response['selected_ptg'] = selected_ptg
    return render(request, 'form update penugasan admin.html', response)

@csrf_exempt
def update_png(request, ktp, tgl_mulai, stasiun):
    if (request.method == 'POST'):
        cursor = connection.cursor()
        ktp_updated = request.POST["petugas"]
        tgl_mulai_updated = request.POST["tgl_mulai"]
        tgl_akhir_updated = request.POST["tgl_akhir"]
        stasiun_updated = request.POST["stasiun"]
        cursor.execute("UPDATE BIKE_SHARING.PENUGASAN SET ktp = '"+ktp_updated+"', start_datetime = '"+tgl_mulai_updated+"', id_stasiun = '"+str(stasiun_updated)+"', end_datetime = '"+tgl_akhir_updated+"' WHERE ktp = '"+ktp+"' AND start_datetime = '"+tgl_mulai+"' AND id_stasiun = '"+stasiun+"';")
    return redirect(reverse('penugasan_adm'))

def del_png(request, ktp, tgl_mulai, stasiun):
    response = {}
    cursor = connection.cursor()
    cursor.execute("DELETE FROM BIKE_SHARING.PENUGASAN WHERE ktp = '"+ktp+"' AND start_datetime = '"+tgl_mulai+"' AND id_stasiun = '"+stasiun+"';")
    return redirect(reverse('penugasan_adm'))

def acara_adm(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.ACARA")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'acara admin.html', response)

def add_acara(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT nama, id_stasiun from BIKE_SHARING.STASIUN")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'form acara admin.html', response)

@csrf_exempt
def add_acr(request):
    if (request.method == 'POST'):
        cursor = connection.cursor()
        cursor.execute("SELECT MAX(to_number(id_acara, '9999999999')) from BIKE_SHARING.ACARA")
        jml_acr = cursor.fetchone()
        id_acara = int(jml_acr[0]) + 1
        judul = request.POST["judul"]
        deskripsi = request.POST["deskripsi"]
        gratis = request.POST["gratis"]
        tgl_mulai = request.POST["tgl_mulai"]
        tgl_akhir = request.POST["tgl_akhir"]
        stasiun = request.POST.getlist("stasiun")
        cursor.execute("INSERT INTO BIKE_SHARING.ACARA (id_acara,judul,deskripsi,tgl_mulai,tgl_akhir,is_free) VALUES('"+str(id_acara)+"','"+judul+"','"+deskripsi+"','"+tgl_mulai+"','"+tgl_akhir+"','"+gratis+"')")
        for i in stasiun:
            cursor.execute("INSERT INTO BIKE_SHARING.ACARA_STASIUN (id_stasiun,id_acara) VALUES ('"+str(i)+"','"+str(id_acara)+"')")
    return redirect(reverse('acara_adm'))

def update_acara(request, id):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM BIKE_SHARING.ACARA WHERE id_acara = '"+id+"'")
    update = dictfetchall(cursor)

    cursor.execute("SELECT nama, id_stasiun from BIKE_SHARING.STASIUN")
    stasiun = dictfetchall(cursor)

    cursor.execute("SELECT * FROM BIKE_SHARING.ACARA_STASIUN where id_acara = '"+id+"'")
    selected_stasiun = dictfetchall(cursor)

    response['update'] = update[0]
    response['stasiun'] = stasiun
    response['selected_stasiun'] = [x['id_stasiun'] for x in selected_stasiun]
    return render(request, 'form update acara admin.html', response)

@csrf_exempt
def update_acr(request, id):
    if (request.method == 'POST'):
        cursor = connection.cursor()
        judul = request.POST["judul"]
        deskripsi = request.POST["deskripsi"]
        gratis = request.POST["gratis"]
        tgl_mulai = request.POST["tgl_mulai"]
        tgl_akhir = request.POST["tgl_akhir"]
        stasiun = request.POST.getlist("stasiun")
        # ini update table acara
        cursor.execute("UPDATE BIKE_SHARING.ACARA SET judul = '"+judul+"', deskripsi = '"+deskripsi+"', tgl_mulai = '"+tgl_mulai+"', tgl_akhir = '"+tgl_akhir+"', is_free = '"+gratis+"' WHERE id_acara = '"+id+"';")
        
        # begin update acara_stasiun
        # caranya: 
        #           1. delete dulu yg id nya = id (id itu id_acara yg mau diganti)
        #           2. insert lagi kayak yg di add_acr (dengan iterasi)
        cursor.execute("DELETE FROM BIKE_SHARING.ACARA_STASIUN WHERE id_acara = '"+id+"';")
        for i in stasiun:
            cursor.execute("INSERT INTO BIKE_SHARING.ACARA_STASIUN (id_stasiun,id_acara) VALUES ('"+str(i)+"','"+str(id)+"')")
        # end update acara_stasiun
    return redirect(reverse('acara_adm'))

def del_acr(request, id):
    response = {}
    cursor = connection.cursor()
    cursor.execute("DELETE FROM BIKE_SHARING.ACARA WHERE id_acara = '"+id+"';")
    cursor.execute("DELETE FROM BIKE_SHARING.ACARA_STASIUN WHERE id_acara = '"+id+"';")
    return redirect(reverse('acara_adm'))

def stasiun_agt(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.STASIUN")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'stasiun anggota.html', response)

def stasiun_ptg(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.STASIUN")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'stasiun petugas.html', response)

def stasiun_adm(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.STASIUN")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'stasiun admin.html', response)

def add_stasiun(request):
    response = {}
    return render(request, 'form stasiun admin.html', response)

def sepeda_agt(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.SEPEDA")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'sepeda anggota.html', response)

def sepeda_ptg(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.SEPEDA")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'sepeda petugas.html', response)

def sepeda_adm(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.SEPEDA")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'sepeda admin.html', response)

def add_sepeda(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT nama, id_stasiun from BIKE_SHARING.STASIUN")
    daftar_s= dictfetchall(cursor)
    response['daftar_s'] = daftar_s
    cursor.execute("SELECT no_kartu from BIKE_SHARING.ANGGOTA")
    daftar_p= dictfetchall(cursor)
    response['daftar_p'] = daftar_p
    return render(request, 'form sepeda admin.html', response)

def add_spd(request):
    if (request.method == 'POST'):
        cursor = connection.cursor()
        cursor.execute("SELECT MAX(to_number(nomor, '9999999999')) from BIKE_SHARING.SEPEDA")
        jml_spd = cursor.fetchone()
        nomor = int(jml_spd[0]) + 1
        merk = request.POST["merk"]
        jenis = request.POST["jenis"]
        status = request.POST["status"]
        stasiun = request.POST["stasiun"]
        penyumbang = request.POST["penyumbang"]
        cursor.execute("INSERT INTO BIKE_SHARING.SEPEDA (nomor,merk,jenis,status,id_stasiun,no_kartu_penyumbang) VALUES('"+str(nomor)+"','"+merk+"','"+jenis+"','"+status+"','"+stasiun+"','"+penyumbang+"')")
    return redirect(reverse('sepeda_adm'))

def add_stn(request):
    if (request.method == 'POST'):
        cursor = connection.cursor()
        cursor.execute("SELECT MAX(to_number(id_stasiun, '9999999999')) from BIKE_SHARING.STASIUN")
        jml_stn = cursor.fetchone()
        nomor = int(jml_stn[0]) + 1
        nama = request.POST["nama"]
        alamat = request.POST["alamat"]
        latitude = request.POST["latitude"]
        longitude = request.POST["longitude"]
        cursor.execute("INSERT INTO BIKE_SHARING.STASIUN (id_stasiun,nama,alamat,lat,long) VALUES('"+str(nomor)+"','"+nama+"','"+alamat+"','"+latitude+"','"+longitude+"')")
    return redirect(reverse('stasiun_adm'))

def update_stasiun(request, id):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM BIKE_SHARING.STASIUN WHERE id_stasiun = '"+id+"'")
    update = dictfetchall(cursor)
    response['update'] = update[0]
    return render(request, 'form update stasiun admin.html', response)

def update_sepeda(request, id):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT nama, id_stasiun from BIKE_SHARING.STASIUN")
    daftar_s= dictfetchall(cursor)
    response['daftar_s'] = daftar_s
    cursor.execute("SELECT no_kartu from BIKE_SHARING.ANGGOTA")
    daftar_p= dictfetchall(cursor)
    response['daftar_p'] = daftar_p
    cursor.execute("SELECT * FROM BIKE_SHARING.SEPEDA WHERE nomor = '"+id+"'")
    update = dictfetchall(cursor)
    response['update'] = update[0]
    return render(request, 'form update sepeda admin.html', response)

def update_stn(request, id):
    if (request.method == 'POST'):
        cursor = connection.cursor()
        nama = request.POST["nama"]
        alamat = request.POST["alamat"]
        latitude = request.POST["lat"]
        longitude = request.POST["long"]
        cursor.execute("UPDATE BIKE_SHARING.STASIUN SET nama = '{}', alamat = '{}', lat = {}, long = {} WHERE id_stasiun = '{}'".format(nama, alamat, latitude, longitude, id))
    return redirect(reverse('stasiun_adm'))

def update_spd(request, id):
    if (request.method == 'POST'):
        cursor = connection.cursor()
        merk = request.POST["merk"]
        jenis = request.POST["jenis"]
        status = request.POST["status"]
        stasiun = request.POST["stasiun"]
        penyumbang = request.POST["penyumbang"]
        cursor.execute("UPDATE BIKE_SHARING.SEPEDA SET merk = '{}', jenis = '{}', status = {}, no_kartu_penyumbang = {} WHERE nomor = '{}'".format(merk, jenis, status, penyumbang, id))
    return redirect(reverse('sepeda_adm'))

def del_stn(request, id):
    response = {}
    cursor = connection.cursor()
    cursor.execute("DELETE FROM BIKE_SHARING.STASIUN WHERE id_stasiun = '"+id+"';")
    return redirect(reverse('stasiun_adm'))

def del_spd(request, id):
    response = {}
    cursor = connection.cursor()
    cursor.execute("DELETE FROM BIKE_SHARING.SEPEDA WHERE nomor = '"+id+"';")
    return redirect(reverse('sepeda_adm'))

def laporan(request):
    response = {}
    return render(request, 'laporan petugas.html', response)

def penugasan(request):
    response = {}
    return render(request, 'penugasan petugas.html', response)

def acara(request):
    response = {}
    return render(request, 'acara petugas.html', response)

def voucher(request):
    response = {}
    return render(request, 'voucher petugas.html', response)

def stasiun(request):
    response = {}
    return render(request, 'stasiun petugas.html', response)

def peminjaman(request):
    response = {}
    return render(request, 'peminjaman petugas.html', response)

def sepeda(request):
    response = {}
    return render(request, 'sepeda petugas.html', response)


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def transaksi_agt(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from BIKE_SHARING.TRANSAKSI")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'transaksi anggota.html', response)

def topup_agt(request):
    response = {}
    return render(request, 'topup anggota.html', response)

def laporan_adm(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT L.id_laporan, L.datetime_pinjam, P.nama, PJ.denda, L.status FROM BIKE_SHARING.LAPORAN AS L, BIKE_SHARING.PEMINJAMAN AS PJ, BIKE_SHARING.PERSON AS P, BIKE_SHARING.ANGGOTA AS A WHERE L.no_kartu_anggota = PJ.no_kartu_anggota AND PJ.no_kartu_anggota = A.no_kartu AND A.ktp = P.ktp;")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'laporan admin.html', response)

def laporan_ptg(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT L.id_laporan, L.datetime_pinjam, P.nama, PJ.denda, L.status FROM BIKE_SHARING.LAPORAN AS L, BIKE_SHARING.PEMINJAMAN AS PJ, BIKE_SHARING.PERSON AS P, BIKE_SHARING.ANGGOTA AS A WHERE L.no_kartu_anggota = PJ.no_kartu_anggota AND PJ.no_kartu_anggota = A.no_kartu AND A.ktp = P.ktp;")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'laporan petugas.html', response)

@csrf_exempt
def add_transaksi(request):
    response = {}
    if (request.method=="POST"):
        try:
            cursor = connection.cursor()
            cursor.execute('set search_path to BIKE_SHARING,public')
            cursor.execute("DROP FUNCTION update_saldo_anggota() CASCADE;")
        except:
            ktp = request.session['ktp']
            # ktp = "111222333"
            cursor = connection.cursor()
            cursor.execute("SELECT no_kartu FROM BIKE_SHARING.ANGGOTA AS A WHERE A.ktp='"+ktp+"'") 
            nomor = cursor.fetchone()
            no_kartu_anggota = nomor[0]
            date_time = datetime.now()
            jenis = 'TopUp'
            nominal = request.POST["nominal"]
            cursor.execute("INSERT INTO BIKE_SHARING.TRANSAKSI(no_kartu_anggota,date_time,jenis,nominal) VALUES ('"+no_kartu_anggota+"','"+str(date_time)+"','"+jenis+"','"+nominal+"')")
            messages.success(request, "Top up berhasil!")
            return redirect(reverse('transaksi_agt'))
        return redirect(reverse('transaksi_agt'))

def register(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        return render(request, 'index.html')
        
    messages.info(request, "Anda sudah terdaftar dalam sistem. Silakan logout terlebih dahulu!")
    return redirect(reverse(''))

@csrf_exempt
def login(request):
    response = {}
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if request.method == 'POST':
            email = request.POST['email']
            ktp = request.POST['ktp']
            cursor=connection.cursor()
            cursor.execute("SELECT * from BIKE_SHARING.PERSON where EMAIL='"+email+"'")
            select=cursor.fetchone()

            if (select):
                cursor.execute("SELECT ktp from BIKE_SHARING.PERSON where EMAIL='"+email+"'")
                select=cursor.fetchone()
                if select[0] == ktp:
                    request.session['logged_in'] = True
                    request.session.modified = True
                    request.session["email"]=email
                    # nama = 'Nama Admin'

                    # cursor.execute("SELECT * from BIKE_SHARING.PERSON where KTP='"+ktp+"' AND NAMA='"+nama+"'")
                    # admin=cursor.fetchone()
                    ktp_admin ="1234567890"
                    email_admin = "adhiba.mastura@yahoo.com"
                    if (email == email_admin and ktp == ktp_admin):
                        request.session["role"]="admin"
                        messages.success(request, "Anda telah berhasil login ke dalam sistem")
                        return redirect(reverse('index_admin'))

                    cursor.execute("SELECT * from BIKE_SHARING.PETUGAS where KTP='"+ktp+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="petugas"
                        messages.success(request, "Anda telah berhasil login ke dalam sistem")
                        return redirect(reverse('index_petugas'))

                    cursor.execute("SELECT * from BIKE_SHARING.ANGGOTA where KTP='"+ktp+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="anggota"
                        request.session["ktp"] = ktp
                        messages.success(request, "Anda telah berhasil login ke dalam sistem")
                        return redirect(reverse('index_anggota'))

                else:
                    messages.error(request, 'Email atau No. KTP salah.')
                    response['email'] = request.POST['email']
                    return render(request, 'index.html', response)
            else:
                messages.error(request, 'Email atau No. KTP salah.')
                response['email'] = request.POST['email']
                return render(request, 'index.html', response)
        else:
            return render(request, 'index.html')

    else:
        messages.info(request,'Anda sudah masuk ke dalam sistem.')
        return redirect(reverse('index'))
                    
def logout(request):
    messages.success(request, 'Anda berhasil keluar dari sistem.')
    request.session.flush()
    return redirect(reverse('index'))

response = {}
@csrf_exempt
def register_petugas(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if(request.method == 'POST'):
            ktp = request.POST["ktp"]
            nama = 'Nama Petugas'
            email = request.POST["email"]
            tgl_lahir = request.POST["tgl_lahir"]
            no_telp = request.POST["no_telp"]
            alamat = request.POST["alamat"]
            gaji = 30000
            kode = request.POST["kode"]

            if (kode == "2007"):
                cursor = connection.cursor()
                cursor.execute("SELECT email from BIKE_SHARING.PERSON where EMAIL='"+email+"'")
                select = cursor.fetchone()
                cursor.execute("SELECT * from BIKE_SHARING.PERSON where KTP='"+ktp+"'")
                check = cursor.fetchone()
                if (check or select):
                    messages.error(request, 'Already registered.')
                    return render(request,'index.html', response)

                cursor.execute("INSERT INTO BIKE_SHARING.PERSON (ktp,email,nama,alamat,tgl_lahir,no_telp) VALUES('"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tgl_lahir+"','"+no_telp+"')")
                cursor.execute("INSERT INTO BIKE_SHARING.PETUGAS (ktp,gaji) VALUES('"+ktp+"','"+str(gaji)+"')")

                request.session["ktp"] = ktp
                request.session["email"] = email
                request.session["nama"] = nama
                request.session["role"] = "petugas"
                request.session["alamat"] = alamat
                request.session["logged_in"] = True
                request.session.modified = True

                messages.success(request, "Anda berhasil registrasi sebagai petugas")
                return redirect(reverse('index_petugas'))
            else:
                messages.success(request, "Anda tidak bisa registrasi sebagai petugas")
                return redirect(reverse('index'))
        return render(request, 'index_petugas.html')
    messages.info(request, "Anda sudah terdaftar dalam sistem. Silakan logout terlebih dahulu!")
    return redirect(reverse('index_petugas'))

@csrf_exempt
def register_anggota(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if(request.method == 'POST'):
            ktp = request.POST["ktp"] 
            nama = 'Nama Anggota'
            email = request.POST["email"]
            tgl_lahir = request.POST["tgl_lahir"]
            no_telp = request.POST["no_telp"]
            alamat = request.POST["alamat"]
            points = 0
            saldo = 0

            cursor = connection.cursor()
            cursor.execute("SELECT MAX(to_number(no_kartu, '9999999999')) from BIKE_SHARING.ANGGOTA")
            jml_kartu = cursor.fetchone()
            no_kartu = int(jml_kartu[0]) + 1
            cursor.execute("SELECT * from BIKE_SHARING.PERSON where EMAIL='"+email+"'")
            select = cursor.fetchone()
            cursor.execute("SELECT * from BIKE_SHARING.PERSON where KTP='"+ktp+"'")
            check = cursor.fetchone()

            if (select or check):
                messages.error(request, 'Already registered.')
                return render(request,'index.html', response) 
            cursor.execute("INSERT INTO BIKE_SHARING.PERSON (ktp,email,nama,alamat,tgl_lahir,no_telp) VALUES('"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tgl_lahir+"','"+no_telp+"')")
            cursor.execute("INSERT INTO BIKE_SHARING.ANGGOTA (no_kartu, saldo, points, ktp) VALUES('"+str(no_kartu)+"','"+str(saldo)+"','"+str(points)+"','"+ktp+"')")

            request.session["ktp"] = ktp
            request.session["email"] = email
            request.session["nama"] = nama
            request.session["role"] = "anggota"
            request.session["alamat"] = alamat
            request.session["logged_in"] = True
            request.session.modified = True

            messages.success(request, "Anda berhasil registrasi sebagai anggota")
            return redirect(reverse('index_anggota'))
        return render(request, 'index_anggota.html')
    messages.info(request, "Anda sudah terdaftar dalam sistem. Silakan logout terlebih dahulu!")
    return redirect(reverse('index_anggota'))

# @csrf_exempt
# def register_admin(request):
#     if 'logged_in' not in request.session or not request.session['logged_in']:
#         if(request.method == 'POST'):
#             ktp = request.POST["ktp"]
#             nama = 'Nama Admin'
#             email = request.POST["email"]
#             tgl_lahir = request.POST["tgl_lahir"]
#             no_telp = request.POST["no_telp"]
#             alamat = request.POST["alamat"]
#             gaji = 30000

#             cursor = connection.cursor()
#             cursor.execute("SELECT email from BIKE_SHARING.PERSON where EMAIL='"+email+"'")
#             select = cursor.fetchone()
#             cursor.execute("SELECT * from BIKE_SHARING.PERSON where KTP='"+ktp+"'")
#             check = cursor.fetchone()
#             if (check or select):
#                 messages.error(request, 'Already registered.')
#                 return render(request,'index.html', response)

#             cursor.execute("INSERT INTO BIKE_SHARING.PERSON (ktp,email,nama,alamat,tgl_lahir,no_telp) VALUES('"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tgl_lahir+"','"+no_telp+"')")
#             cursor.execute("INSERT INTO BIKE_SHARING.PETUGAS (ktp,gaji) VALUES('"+ktp+"','"+str(gaji)+"')")

#             request.session["ktp"] = ktp
#             request.session["email"] = email
#             request.session["nama"] = nama
#             request.session["role"] = "admin"
#             request.session["alamat"] = alamat
#             request.session["logged_in"] = True
#             request.session.modified = True

#             messages.success(request, "Anda berhasil registrasi sebagai admin")
#             return redirect(reverse('index_admin'))
#         return render(request, 'index_admin.html')
#     messages.info(request, "Anda sudah terdaftar dalam sistem. Silakan logout terlebih dahulu!")
#     return redirect(reverse('index_admin'))

