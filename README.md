# TKBasdat

NAMA
1 Registrasi, Login dan Logout (Semua user)
2 Create transaksi topup (Anggota)
3 Melihat riwayat transaksi (Anggota)
4 Melihat daftar laporan (Admin, petugas)

NAMA
5 Create, update, delete (CUD) penugasan (Admin)
6 Create, update, delete (CUD) acara (Admin)
7 Melihat daftar penugasan (Admin, petugas)
8 Melihat daftar acara (Semua user)

NAMA
9 Create, update, delete (CUD) stasiun (Admin)
10 Create, update, delete (CUD) sepeda (Admin)
11 Melihat daftar stasiun (Semua user)
12 Melihat daftar sepeda (Semua user)

NAMA
13 Create, update, delete (CUD) voucher (Admin)
14 Create, update (CU) peminjaman (Anggota)
15 Melihat daftar voucher (Semua user)
16 Melihat daftar peminjaman (Semua user)